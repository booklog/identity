# Identity Service

[![Build Status](https://gitlab.com/booklog/identity/badges/master/build.svg)](https://gitlab.com/booklog/identity/commits/master)

Identity service provides authentication and authorization
services. The service operates using gRPC.
