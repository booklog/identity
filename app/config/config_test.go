package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseArgs(t *testing.T) {
	tests := []struct {
		name string
	}{
		{"default-settings"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := ParseArgs()
			assert.Equal(t, DefaultPort, got.Port, tt.name)
			assert.Equal(t, DefaultDriver, got.Driver, tt.name)
		})
	}
}
