package datastore

import (
	_ "github.com/mattn/go-sqlite3" // sqlite driver
	"gitlab.com/booklog/identity/app/entity"
)

// Datastore interface for the database logic
type Datastore interface {
	Login(user, password string) (entity.UserInfo, error)
}
