package memory

import (
	"errors"

	"gitlab.com/booklog/identity/app/entity"
)

type user struct {
	user     string
	password string
	name     string
	email    string
	role     int
}

// Datastore is a memory datastore
type Datastore struct {
	users []user
}

// NewStore creates a new memory store
func NewStore() *Datastore {
	return &Datastore{
		users: []user{{"john", "secret", "John Doe", "john@example.com", 100}},
	}
}

// Login checks the user/password
func (ds *Datastore) Login(user, password string) (entity.UserInfo, error) {
	uu := entity.UserInfo{}
	for _, u := range ds.users {
		if (u.user == user) && (u.password == password) {
			uu.Username = u.user
			uu.Role = u.role
			uu.Email = u.email
			uu.Name = u.name
			return uu, nil
		}
	}
	return uu, errors.New("user and password not found")
}
