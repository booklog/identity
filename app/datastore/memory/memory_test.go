package memory

import (
	"reflect"
	"testing"

	"gitlab.com/booklog/identity/app/entity"
)

func TestDatastore_Login(t *testing.T) {
	req1 := entity.UserInfo{
		Username: "john",
		Name:     "John Doe",
		Email:    "john@example.com",
		Role:     100,
	}
	type args struct {
		user     string
		password string
	}
	tests := []struct {
		name    string
		args    args
		want    entity.UserInfo
		wantErr bool
	}{
		{"valid", args{"john", "secret"}, req1, false},
		{"invalid", args{"john", "ignore"}, req1, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ds := NewStore()
			got, err := ds.Login(tt.args.user, tt.args.password)
			if (err != nil) != tt.wantErr {
				t.Errorf("Datastore.Login() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr {
				if !reflect.DeepEqual(got, tt.want) {
					t.Errorf("Datastore.Login() = %v, want %v", got, tt.want)
				}
			}
		})
	}
}
