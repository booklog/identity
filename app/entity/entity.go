package entity

// UserInfo holds the user details
type UserInfo struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Name     string `json:"name"`
	Role     int    `json:"role"`
}
