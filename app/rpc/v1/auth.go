package v1

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/booklog/identity/app/rpc/v1/proto/userservice"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// Login handler
func (srv Controller) Login(ctx context.Context, in *userservice.LoginRequest) (*userservice.LoginReply, error) {
	out := userservice.LoginReply{}

	u, err := srv.AuthService.Login(in.User, in.Password)
	if err != nil {
		log.Printf("Login failed for '%s': %v", in.User, err)
		return &out, status.Error(codes.NotFound, err.Error())
	}

	// Set the user details in the output
	out.User = &userservice.UserInfo{
		User:  u.Username,
		Name:  u.Name,
		Email: u.Email,
	}
	out.User.Role = getRole(u.Role)

	// Create the JWT from the user details and store it in the reply
	token, err := srv.NewJWTToken(out.User, time.Now().Add(time.Hour*24).Unix())
	if err != nil {
		log.Printf("Error creating the JWT: %v", err)
		return &out, status.Error(codes.FailedPrecondition, "Error creating the JWT")
	}
	out.Token = token
	log.Println(token)

	log.Printf("Login for '%s' successful", u.Username)
	return &out, nil
}

// Verify checks a token and returns the user details
func (srv Controller) Verify(ctx context.Context, in *userservice.VerifyRequest) (*userservice.VerifyReply, error) {
	out := userservice.VerifyReply{}

	// Decode the token
	t, err := srv.VerifyJWT(in.Token)
	if err != nil {
		return &out, err
	}

	// Get the details from the token claims
	claims, ok := t.Claims.(jwt.MapClaims)
	if !ok || !t.Valid {
		return &out, fmt.Errorf("the token is not valid")
	}

	out.User = &userservice.UserInfo{
		User:  claims[ClaimsUsername].(string),
		Email: claims[ClaimsEmail].(string),
		Name:  claims[ClaimsName].(string),
		Role:  getRole(int(claims[ClaimsRole].(float64))),
	}
	return &out, nil
}

func getRole(role int) userservice.UserInfo_Role {
	switch role {
	case 900:
		return userservice.UserInfo_ADMIN
	case 100:
		return userservice.UserInfo_STANDARD
	default:
		return userservice.UserInfo_INVALID
	}
}
