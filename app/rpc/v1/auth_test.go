package v1

import (
	"context"
	"testing"
	"time"

	"gitlab.com/booklog/identity/app/config"
	"gitlab.com/booklog/identity/app/rpc/v1/proto/userservice"
)

var settings = config.ParseArgs()

func TestController_Login(t *testing.T) {
	user1 := &userservice.LoginRequest{
		User: "john", Password: "secret",
	}
	user2 := &userservice.LoginRequest{
		User: "john", Password: "invalid",
	}

	type args struct {
		ctx context.Context
		in  *userservice.LoginRequest
	}
	tests := []struct {
		name     string
		args     args
		username string
		role     userservice.UserInfo_Role
		wantErr  bool
	}{
		{"valid", args{context.Background(), user1}, "John Doe", userservice.UserInfo_STANDARD, false},
		{"invalid", args{context.Background(), user2}, "", userservice.UserInfo_STANDARD, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			srv := NewServer(settings, &mockAuth{})
			got, err := srv.Login(tt.args.ctx, tt.args.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("Controller.Login() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !tt.wantErr {
				if got.User.Name != tt.username {
					t.Errorf("Controller.Login() error = %v, want %v", got.User.Name, tt.username)
				}
				if got.User.Role != tt.role {
					t.Errorf("Controller.Login() error = %v, want %v", got.User.Role, tt.role)
				}
			}

		})
	}
}

func TestController_Verify(t *testing.T) {
	//req1 := &userservice.VerifyRequest{
	//	Token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImpvaG5AZXhhbXBsZS5jb20iLCJleHAiOjE1NTM5ODE4NjMsIm5hbWUiOiJKb2huIERvZSIsInJvbGUiOjEwMCwidXNlcm5hbWUiOiJqb2huIn0.VRFvLsYE7sZ3HoFaOhf4JG-CBYZrgtF9tR_EmKfBZhU",
	//}
	//req2 := &userservice.VerifyRequest{
	//	Token: "invalid.eyJlbWFpbCI6ImpvaG5AZXhhbXBsZS5jb20iLCJleHAiOjE1NTM5ODE4NjMsIm5hbWUiOiJKb2huIERvZSIsInJvbGUiOjEwMCwidXNlcm5hbWUiOiJqb2huIn0.VRFvLsYE7sZ3HoFaOhf4JG-CBYZrgtF9tR_EmKfBZhU",
	//}

	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name     string
		args     args
		interval time.Duration
		username string

		role    userservice.UserInfo_Role
		wantErr bool
	}{
		{"valid", args{context.Background()}, 24, "John Doe", userservice.UserInfo_STANDARD, false},
		{"invalid-interval", args{context.Background()}, -24, "", userservice.UserInfo_STANDARD, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			srv := NewServer(settings, &mockAuth{})

			// Create the token with the requested interval
			user := &userservice.UserInfo{
				User: tt.name,
				Name: tt.username,
				Role: tt.role,
			}
			token, err := srv.NewJWTToken(user, time.Now().Add(time.Hour*tt.interval).Unix())
			if err != nil {
				t.Errorf("Controller.Verify() error token = %v, wantErr %v", err, nil)
				return
			}
			req := &userservice.VerifyRequest{Token: token}

			// Verify the token
			got, err := srv.Verify(tt.args.ctx, req)
			if (err != nil) != tt.wantErr {
				t.Errorf("Controller.Verify() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr {
				if got.User.Name != tt.username {
					t.Errorf("Controller.Verify() error = %v, want %v", got.User.Name, tt.username)
				}
				if got.User.Role != tt.role {
					t.Errorf("Controller.Verify() error = %v, want %v", got.User.Role, tt.role)
				}
			}
		})
	}
}
