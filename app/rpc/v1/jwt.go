package v1

import (
	"errors"
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/booklog/identity/app/rpc/v1/proto/userservice"
	"log"
)

// UserClaims holds the JWT custom claims for a user
const (
	ClaimsUsername         = "username"
	ClaimsEmail            = "email"
	ClaimsName             = "name"
	ClaimsRole             = "role"
	StandardClaimExpiresAt = "exp"
)

// NewJWTToken creates a new JWT from the login response
func (srv Controller) NewJWTToken(userinfo *userservice.UserInfo, expires int64) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)
	claims[ClaimsUsername] = userinfo.User
	claims[ClaimsName] = userinfo.Name
	claims[ClaimsEmail] = userinfo.Email
	claims[ClaimsRole] = userinfo.Role
	claims[StandardClaimExpiresAt] = expires

	if len(srv.Settings.JWTSecret) == 0 {
		return "", errors.New("JWT secret empty value. Please configure it properly")
	}

	tokenString, err := token.SignedString([]byte(srv.Settings.JWTSecret))
	if err != nil {
		log.Printf("Error signing the JWT: %v", err.Error())
	}
	return tokenString, err
}

func (srv Controller) keyFunc(token *jwt.Token) (interface{}, error) {
	if len(srv.Settings.JWTSecret) == 0 {
		return []byte{}, errors.New("JWT secret empty value. Please configure it properly")
	}
	return []byte(srv.Settings.JWTSecret), nil
}

// VerifyJWT checks that we have a valid token
func (srv Controller) VerifyJWT(jwtToken string) (*jwt.Token, error) {
	token, err := jwt.Parse(jwtToken, srv.keyFunc)
	return token, err
}
