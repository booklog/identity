// Code generated by protoc-gen-go. DO NOT EDIT.
// source: proto/userservice/userservice.proto

package userservice

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type UserInfo_Role int32

const (
	UserInfo_INVALID  UserInfo_Role = 0
	UserInfo_STANDARD UserInfo_Role = 100
	UserInfo_ADMIN    UserInfo_Role = 900
)

var UserInfo_Role_name = map[int32]string{
	0:   "INVALID",
	100: "STANDARD",
	900: "ADMIN",
}

var UserInfo_Role_value = map[string]int32{
	"INVALID":  0,
	"STANDARD": 100,
	"ADMIN":    900,
}

func (x UserInfo_Role) String() string {
	return proto.EnumName(UserInfo_Role_name, int32(x))
}

func (UserInfo_Role) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_7cfec0cc22371d20, []int{1, 0}
}

// The request message containing the login credentials
type LoginRequest struct {
	User                 string   `protobuf:"bytes,1,opt,name=user,proto3" json:"user,omitempty"`
	Password             string   `protobuf:"bytes,2,opt,name=password,proto3" json:"password,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *LoginRequest) Reset()         { *m = LoginRequest{} }
func (m *LoginRequest) String() string { return proto.CompactTextString(m) }
func (*LoginRequest) ProtoMessage()    {}
func (*LoginRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_7cfec0cc22371d20, []int{0}
}

func (m *LoginRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_LoginRequest.Unmarshal(m, b)
}
func (m *LoginRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_LoginRequest.Marshal(b, m, deterministic)
}
func (m *LoginRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_LoginRequest.Merge(m, src)
}
func (m *LoginRequest) XXX_Size() int {
	return xxx_messageInfo_LoginRequest.Size(m)
}
func (m *LoginRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_LoginRequest.DiscardUnknown(m)
}

var xxx_messageInfo_LoginRequest proto.InternalMessageInfo

func (m *LoginRequest) GetUser() string {
	if m != nil {
		return m.User
	}
	return ""
}

func (m *LoginRequest) GetPassword() string {
	if m != nil {
		return m.Password
	}
	return ""
}

// The request message containing the user details
type UserInfo struct {
	User                 string        `protobuf:"bytes,1,opt,name=user,proto3" json:"user,omitempty"`
	Name                 string        `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Email                string        `protobuf:"bytes,3,opt,name=email,proto3" json:"email,omitempty"`
	Role                 UserInfo_Role `protobuf:"varint,4,opt,name=role,proto3,enum=UserInfo_Role" json:"role,omitempty"`
	XXX_NoUnkeyedLiteral struct{}      `json:"-"`
	XXX_unrecognized     []byte        `json:"-"`
	XXX_sizecache        int32         `json:"-"`
}

func (m *UserInfo) Reset()         { *m = UserInfo{} }
func (m *UserInfo) String() string { return proto.CompactTextString(m) }
func (*UserInfo) ProtoMessage()    {}
func (*UserInfo) Descriptor() ([]byte, []int) {
	return fileDescriptor_7cfec0cc22371d20, []int{1}
}

func (m *UserInfo) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UserInfo.Unmarshal(m, b)
}
func (m *UserInfo) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UserInfo.Marshal(b, m, deterministic)
}
func (m *UserInfo) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UserInfo.Merge(m, src)
}
func (m *UserInfo) XXX_Size() int {
	return xxx_messageInfo_UserInfo.Size(m)
}
func (m *UserInfo) XXX_DiscardUnknown() {
	xxx_messageInfo_UserInfo.DiscardUnknown(m)
}

var xxx_messageInfo_UserInfo proto.InternalMessageInfo

func (m *UserInfo) GetUser() string {
	if m != nil {
		return m.User
	}
	return ""
}

func (m *UserInfo) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *UserInfo) GetEmail() string {
	if m != nil {
		return m.Email
	}
	return ""
}

func (m *UserInfo) GetRole() UserInfo_Role {
	if m != nil {
		return m.Role
	}
	return UserInfo_INVALID
}

// The response message from a login
type LoginReply struct {
	User                 *UserInfo `protobuf:"bytes,1,opt,name=user,proto3" json:"user,omitempty"`
	Token                string    `protobuf:"bytes,2,opt,name=Token,proto3" json:"Token,omitempty"`
	XXX_NoUnkeyedLiteral struct{}  `json:"-"`
	XXX_unrecognized     []byte    `json:"-"`
	XXX_sizecache        int32     `json:"-"`
}

func (m *LoginReply) Reset()         { *m = LoginReply{} }
func (m *LoginReply) String() string { return proto.CompactTextString(m) }
func (*LoginReply) ProtoMessage()    {}
func (*LoginReply) Descriptor() ([]byte, []int) {
	return fileDescriptor_7cfec0cc22371d20, []int{2}
}

func (m *LoginReply) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_LoginReply.Unmarshal(m, b)
}
func (m *LoginReply) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_LoginReply.Marshal(b, m, deterministic)
}
func (m *LoginReply) XXX_Merge(src proto.Message) {
	xxx_messageInfo_LoginReply.Merge(m, src)
}
func (m *LoginReply) XXX_Size() int {
	return xxx_messageInfo_LoginReply.Size(m)
}
func (m *LoginReply) XXX_DiscardUnknown() {
	xxx_messageInfo_LoginReply.DiscardUnknown(m)
}

var xxx_messageInfo_LoginReply proto.InternalMessageInfo

func (m *LoginReply) GetUser() *UserInfo {
	if m != nil {
		return m.User
	}
	return nil
}

func (m *LoginReply) GetToken() string {
	if m != nil {
		return m.Token
	}
	return ""
}

// The request message to verify a token
type VerifyRequest struct {
	Token                string   `protobuf:"bytes,1,opt,name=token,proto3" json:"token,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *VerifyRequest) Reset()         { *m = VerifyRequest{} }
func (m *VerifyRequest) String() string { return proto.CompactTextString(m) }
func (*VerifyRequest) ProtoMessage()    {}
func (*VerifyRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_7cfec0cc22371d20, []int{3}
}

func (m *VerifyRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_VerifyRequest.Unmarshal(m, b)
}
func (m *VerifyRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_VerifyRequest.Marshal(b, m, deterministic)
}
func (m *VerifyRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_VerifyRequest.Merge(m, src)
}
func (m *VerifyRequest) XXX_Size() int {
	return xxx_messageInfo_VerifyRequest.Size(m)
}
func (m *VerifyRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_VerifyRequest.DiscardUnknown(m)
}

var xxx_messageInfo_VerifyRequest proto.InternalMessageInfo

func (m *VerifyRequest) GetToken() string {
	if m != nil {
		return m.Token
	}
	return ""
}

// The response message from token verification
type VerifyReply struct {
	User                 *UserInfo `protobuf:"bytes,1,opt,name=user,proto3" json:"user,omitempty"`
	Valid                bool      `protobuf:"varint,2,opt,name=valid,proto3" json:"valid,omitempty"`
	XXX_NoUnkeyedLiteral struct{}  `json:"-"`
	XXX_unrecognized     []byte    `json:"-"`
	XXX_sizecache        int32     `json:"-"`
}

func (m *VerifyReply) Reset()         { *m = VerifyReply{} }
func (m *VerifyReply) String() string { return proto.CompactTextString(m) }
func (*VerifyReply) ProtoMessage()    {}
func (*VerifyReply) Descriptor() ([]byte, []int) {
	return fileDescriptor_7cfec0cc22371d20, []int{4}
}

func (m *VerifyReply) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_VerifyReply.Unmarshal(m, b)
}
func (m *VerifyReply) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_VerifyReply.Marshal(b, m, deterministic)
}
func (m *VerifyReply) XXX_Merge(src proto.Message) {
	xxx_messageInfo_VerifyReply.Merge(m, src)
}
func (m *VerifyReply) XXX_Size() int {
	return xxx_messageInfo_VerifyReply.Size(m)
}
func (m *VerifyReply) XXX_DiscardUnknown() {
	xxx_messageInfo_VerifyReply.DiscardUnknown(m)
}

var xxx_messageInfo_VerifyReply proto.InternalMessageInfo

func (m *VerifyReply) GetUser() *UserInfo {
	if m != nil {
		return m.User
	}
	return nil
}

func (m *VerifyReply) GetValid() bool {
	if m != nil {
		return m.Valid
	}
	return false
}

func init() {
	proto.RegisterEnum("UserInfo_Role", UserInfo_Role_name, UserInfo_Role_value)
	proto.RegisterType((*LoginRequest)(nil), "LoginRequest")
	proto.RegisterType((*UserInfo)(nil), "UserInfo")
	proto.RegisterType((*LoginReply)(nil), "LoginReply")
	proto.RegisterType((*VerifyRequest)(nil), "VerifyRequest")
	proto.RegisterType((*VerifyReply)(nil), "VerifyReply")
}

func init() {
	proto.RegisterFile("proto/userservice/userservice.proto", fileDescriptor_7cfec0cc22371d20)
}

var fileDescriptor_7cfec0cc22371d20 = []byte{
	// 325 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x8c, 0x91, 0xc1, 0x6a, 0xc2, 0x40,
	0x10, 0x86, 0x4d, 0x9b, 0x68, 0x9c, 0xa8, 0xc8, 0xe0, 0x21, 0x08, 0x05, 0xd9, 0x22, 0x78, 0xe9,
	0x16, 0xec, 0xbd, 0x10, 0xc9, 0x25, 0x60, 0x3d, 0xa4, 0xd6, 0x42, 0x6f, 0x69, 0x1d, 0x4b, 0x68,
	0xcc, 0xda, 0x5d, 0xb5, 0x78, 0xef, 0x5b, 0xf4, 0x65, 0x8b, 0xbb, 0x89, 0x4d, 0xa1, 0x87, 0xde,
	0xe6, 0xdf, 0xf9, 0x67, 0xe7, 0x9b, 0x19, 0xb8, 0xdc, 0x48, 0xb1, 0x15, 0xd7, 0x3b, 0x45, 0x52,
	0x91, 0xdc, 0xa7, 0x2f, 0x54, 0x8d, 0xb9, 0xce, 0xb2, 0x5b, 0x68, 0x4d, 0xc5, 0x6b, 0x9a, 0xc7,
	0xf4, 0xbe, 0x23, 0xb5, 0x45, 0x04, 0xfb, 0x68, 0xf2, 0xad, 0x81, 0x35, 0x6a, 0xc6, 0x3a, 0xc6,
	0x3e, 0xb8, 0x9b, 0x44, 0xa9, 0x0f, 0x21, 0x97, 0xfe, 0x99, 0x7e, 0x3f, 0x69, 0xf6, 0x65, 0x81,
	0xfb, 0xa0, 0x48, 0x46, 0xf9, 0x4a, 0xfc, 0x59, 0x8c, 0x60, 0xe7, 0xc9, 0x9a, 0x8a, 0x42, 0x1d,
	0x63, 0x0f, 0x1c, 0x5a, 0x27, 0x69, 0xe6, 0x9f, 0xeb, 0x47, 0x23, 0x90, 0x81, 0x2d, 0x45, 0x46,
	0xbe, 0x3d, 0xb0, 0x46, 0x9d, 0x71, 0x87, 0x97, 0xdf, 0xf2, 0x58, 0x64, 0x14, 0xeb, 0x1c, 0xbb,
	0x02, 0xfb, 0xa8, 0xd0, 0x83, 0x46, 0x34, 0x5b, 0x04, 0xd3, 0x28, 0xec, 0xd6, 0xb0, 0x05, 0xee,
	0xfd, 0x3c, 0x98, 0x85, 0x41, 0x1c, 0x76, 0x97, 0x08, 0xe0, 0x04, 0xe1, 0x5d, 0x34, 0xeb, 0x7e,
	0x36, 0x58, 0x00, 0x50, 0x4c, 0xb7, 0xc9, 0x0e, 0x78, 0x51, 0xc1, 0xf3, 0xc6, 0xcd, 0x53, 0x83,
	0x82, 0xb4, 0x07, 0xce, 0x5c, 0xbc, 0x51, 0x5e, 0xa0, 0x1a, 0xc1, 0x86, 0xd0, 0x5e, 0x90, 0x4c,
	0x57, 0x87, 0x72, 0x43, 0x3d, 0x70, 0xb6, 0xda, 0x66, 0xa6, 0x34, 0x82, 0x4d, 0xc0, 0x2b, 0x6d,
	0xff, 0x6b, 0xb5, 0x4f, 0xb2, 0xd4, 0xac, 0xd3, 0x8d, 0x8d, 0x18, 0x3f, 0x82, 0x7d, 0xf4, 0xe1,
	0x10, 0x1c, 0x4d, 0x8d, 0x6d, 0x5e, 0xbd, 0x4d, 0xdf, 0xe3, 0x3f, 0xc3, 0xb0, 0x1a, 0x8e, 0xa0,
	0x6e, 0x5a, 0x62, 0x87, 0xff, 0x42, 0xec, 0xb7, 0x78, 0x85, 0x85, 0xd5, 0x26, 0xed, 0x27, 0xaf,
	0x72, 0xf9, 0xe7, 0xba, 0x3e, 0xfd, 0xcd, 0x77, 0x00, 0x00, 0x00, 0xff, 0xff, 0xe4, 0x6d, 0x36,
	0x6a, 0x21, 0x02, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// UserClient is the client API for User service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type UserClient interface {
	// Login an existing user
	Login(ctx context.Context, in *LoginRequest, opts ...grpc.CallOption) (*LoginReply, error)
	Verify(ctx context.Context, in *VerifyRequest, opts ...grpc.CallOption) (*VerifyReply, error)
}

type userClient struct {
	cc *grpc.ClientConn
}

func NewUserClient(cc *grpc.ClientConn) UserClient {
	return &userClient{cc}
}

func (c *userClient) Login(ctx context.Context, in *LoginRequest, opts ...grpc.CallOption) (*LoginReply, error) {
	out := new(LoginReply)
	err := c.cc.Invoke(ctx, "/User/Login", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userClient) Verify(ctx context.Context, in *VerifyRequest, opts ...grpc.CallOption) (*VerifyReply, error) {
	out := new(VerifyReply)
	err := c.cc.Invoke(ctx, "/User/Verify", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// UserServer is the server API for User service.
type UserServer interface {
	// Login an existing user
	Login(context.Context, *LoginRequest) (*LoginReply, error)
	Verify(context.Context, *VerifyRequest) (*VerifyReply, error)
}

func RegisterUserServer(s *grpc.Server, srv UserServer) {
	s.RegisterService(&_User_serviceDesc, srv)
}

func _User_Login_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(LoginRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserServer).Login(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/User/Login",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserServer).Login(ctx, req.(*LoginRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _User_Verify_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(VerifyRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserServer).Verify(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/User/Verify",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserServer).Verify(ctx, req.(*VerifyRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _User_serviceDesc = grpc.ServiceDesc{
	ServiceName: "User",
	HandlerType: (*UserServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Login",
			Handler:    _User_Login_Handler,
		},
		{
			MethodName: "Verify",
			Handler:    _User_Verify_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "proto/userservice/userservice.proto",
}
