package v1

import (
	"gitlab.com/booklog/identity/app/config"
	"gitlab.com/booklog/identity/app/datastore"
	"gitlab.com/booklog/identity/app/service"
)

// Controller is the API web service
type Controller struct {
	Settings    *config.Settings
	AuthService service.AuthService
}

// NewServer returns a new web controller
func NewServer(settings *config.Settings, db datastore.Datastore) *Controller {
	return &Controller{
		Settings:    settings,
		AuthService: service.NewAuthService(db),
	}
}
