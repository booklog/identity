package v1

import (
	"fmt"
	"gitlab.com/booklog/identity/app/entity"
)

type mockAuth struct{}

var userJohn = entity.UserInfo{
	Username: "john",
	Name:     "John Doe",
	Email:    "john@example.com",
	Role:     100,
}

func (mockAuth) Login(user, password string) (entity.UserInfo, error) {
	if user == "john" && password == "secret" {
		return userJohn, nil
	}
	return entity.UserInfo{}, fmt.Errorf("MOCK login error")
}
