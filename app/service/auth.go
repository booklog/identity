package service

import (
	"gitlab.com/booklog/identity/app/datastore"
	"gitlab.com/booklog/identity/app/entity"
)

// AuthService provides APIs for authn and authz
type AuthService interface {
	Login(user, password string) (entity.UserInfo, error)
}

type authAdapter struct {
	DB datastore.Datastore
}

// NewAuthService creates a auth service
func NewAuthService(db datastore.Datastore) AuthService {
	return &authAdapter{DB: db}
}

func (aa *authAdapter) Login(user, password string) (entity.UserInfo, error) {
	return aa.DB.Login(user, password)
}
