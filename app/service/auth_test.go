package service

import (
	"gitlab.com/booklog/identity/app/datastore/memory"
	"reflect"
	"testing"

	"gitlab.com/booklog/identity/app/datastore"
	"gitlab.com/booklog/identity/app/entity"
)

func Test_authAdapter_Login(t *testing.T) {
	db := memory.NewStore()
	user := entity.UserInfo{
		Username: "john",
		Name:     "John Doe",
		Email:    "john@example.com",
		Role:     100,
	}
	type fields struct {
		DB datastore.Datastore
	}
	type args struct {
		user     string
		password string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    entity.UserInfo
		wantErr bool
	}{
		{"valid", fields{db}, args{"john", "secret"}, user, false},
		{"invalid", fields{db}, args{"john", "invalid"}, entity.UserInfo{}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			aa := NewAuthService(tt.fields.DB)
			got, err := aa.Login(tt.args.user, tt.args.password)
			if (err != nil) != tt.wantErr {
				t.Errorf("authAdapter.Login() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("authAdapter.Login() = %v, want %v", got, tt.want)
			}
		})
	}
}
