package main

import (
	"fmt"
	"gitlab.com/booklog/identity/app/config"
	"gitlab.com/booklog/identity/app/datastore/memory"
	"gitlab.com/booklog/identity/app/rpc/v1"
	"gitlab.com/booklog/identity/app/rpc/v1/proto/userservice"
	"google.golang.org/grpc"
	"log"
	"net"
)

func main() {
	// Set up the dependency chain
	settings := config.ParseArgs()
	db := memory.NewStore()
	ctrl := v1.NewServer(settings, db)

	// Setup the listener
	lis, err := net.Listen("tcp", fmt.Sprintf("localhost%s", settings.Port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	// Start the gRPC user service
	log.Println("Starting Identity Service on port", settings.Port)
	rpcServer := grpc.NewServer()
	userservice.RegisterUserServer(rpcServer, ctrl)
	log.Fatal(rpcServer.Serve(lis))
}
