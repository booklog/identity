module gitlab.com/booklog/identity

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/golang/protobuf v1.3.1
	github.com/mattn/go-sqlite3 v0.0.0-20190217174029-ad30583d8387
	github.com/stretchr/testify v0.0.0-20190228214809-21cb1c2932a2
	gitlab.com/booklog/booklogweb v0.0.0-20190328224700-2bbe0e4c62d2 // indirect
	google.golang.org/grpc v1.19.0
)
